/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usermanagment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import usermanagment.User;

/**
 *
 * @author acer
 */
public class Testreadfile {
     public static void main(String[] args) {
             ArrayList<User> list = new ArrayList();
             File file = null;
             FileInputStream fis = null;
             ObjectInputStream ois = null;
            try {          
             file = new File("user.bin");
             fis = new  FileInputStream(file);
             ois = new ObjectInputStream(fis);
            list = (ArrayList<User>)ois.readObject();
         
         } catch (FileNotFoundException ex) {
             
         } catch (IOException ex) {
             
         } catch (ClassNotFoundException ex) {
             
         }
            for(User u : list){
                System.out.println(u);
            }
             
     }
}
