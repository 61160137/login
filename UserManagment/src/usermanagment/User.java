/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usermanagment;

import java.io.Serializable;

/**
 *
 * @author Lenovo
 */
public class User implements Serializable {
    private String loginName;
    private String password;

    User(String user, String password) {
        loginName = user;
        this.password = password;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "loginName=" + loginName + ", password=" + password + '}';
    }
    
    
}
