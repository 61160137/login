/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxswing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class Testreadfile {
     public static void main(String[] args) {

             File file = null;
             Player o,x;
             FileInputStream fis = null;
             ObjectInputStream ois = null;
            try {          
             file = new File("ox.bin");
             fis = new  FileInputStream(file);
             ois = new ObjectInputStream(fis);
             o = (Player)ois.readObject();
             x = (Player)ois.readObject();
             System.out.println(o);
             System.out.println(x);       
         } catch (FileNotFoundException ex) {
             Logger.getLogger(Testreadfile.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(Testreadfile.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(Testreadfile.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
}
